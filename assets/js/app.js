// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css";

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html";

var table = document.getElementById("list_nodes"),
    i = 1;
var table_entries = document.getElementById("list_table").innerHTML,
    json_table_entries = JSON.parse(table_entries);
var row = table.insertRow(0);
var cell1 = row.insertCell(0);
var cell2 = row.insertCell(1);
var cell3 = row.insertCell(2);
var rows = [];
var unspent_transaction_amount = document.getElementById("td_unspent_amount");
var show_transaction_form = document.getElementById("show_transaction_form");
var address_index_map = {};
var overlay_chart = document.getElementById("overlay_chart");
var charts = document.getElementById("charts");
var request_charts = false;

// overlay_chart.onclick = function () {
//     overlay_chart.style.display = "none";
//     request_charts = false;
// };

charts.onclick = function () {
    if (document.getElementById("main_stats").hidden === true) {
        document.getElementById("main_stats").hidden = false;
        document.getElementById("charts_div").hidden = true;
        // document.getElementById("charts_button").style = "border-style:inset;";
        request_charts = false;
    } else {
        document.getElementById("main_stats").hidden = true;
        document.getElementById("charts_div").hidden = false;
        request_charts = true;
    }
};

cell1.innerHTML = "Index";
cell2.innerHTML = "Wallet Address";
cell3.innerHTML = "Balance";

// console.log(json_table_entries);
if (show_transaction_form.innerHTML === "true") {
    document.getElementById("transaction_form").hidden = false;
}

for (i = 1; i <= json_table_entries.length; i++) {
    // console.log(json_table_entries[i]);
    row = table.insertRow(i);
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);
    cell3 = row.insertCell(2);
    cell1.innerHTML = i;
    cell2.innerHTML = json_table_entries[i - 1].address;
    cell3.innerHTML = json_table_entries[i - 1].balance;
    rows.push(row);
}

function ajaxCalls(url, callFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            callFunction(this);
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
}

function table_nodes(xhr) {
    var json_response = JSON.parse(xhr.response);
    // console.log(json_response);
    for (i = 1; i <= json_table_entries.length; i++) {
        document.getElementById("list_nodes").rows[i].cells[1].innerHTML = i;
        document.getElementById("list_nodes").rows[i].cells[1].innerHTML = json_response[i - 1].address;
        address_index_map[json_response[i - 1].address] = i;
        document.getElementById("list_nodes").rows[i].cells[2].innerHTML = json_response[i - 1].balance;
    }
}

function num_transactions(xhr) {
    var json_response = JSON.parse(xhr.response);
    // console.log(json_response);
    document.getElementById("td_num_transactions").innerHTML = json_response.num_transactions;
}

function mined_bitcoin_balance(xhr) {
    var json_response = JSON.parse(xhr.response);
    // console.log(json_response);
    document.getElementById("td_bitcoin_mined").innerHTML = json_response.mined_bitcoin_balance;
}

function transacted_bitcoin_balance(xhr) {
    var json_response = JSON.parse(xhr.response);
    // console.log(json_response);
    document.getElementById("td_bitcoin_transacted").innerHTML = json_response.transacted_bitcoin_balance;
}

function utxos(xhr) {
    var json_response = JSON.parse(xhr.response),
        j = 1;
    // console.log(json_response);
    var utxo_table = document.getElementById("utxo_list");
    utxo_table.innerHTML = "";
    var utxo_row = utxo_table.insertRow(0);
    var utxo_cell0 = utxo_row.insertCell(0);
    var utxo_cell1 = utxo_row.insertCell(1);
    var utxo_cell2 = utxo_row.insertCell(2);
    var utxo_amount = 0;
    utxo_cell0.innerHTML = "Index";
    utxo_cell1.innerHTML = "Address";
    utxo_cell2.innerHTML = "Amount";
    for (j = 1; j <= json_response.length; j++) {
        if (json_response[j - 1].amount != 0) {
            utxo_row = utxo_table.insertRow(j);
            utxo_cell0 = utxo_row.insertCell(0);
            utxo_cell1 = utxo_row.insertCell(1);
            utxo_cell2 = utxo_row.insertCell(2);
            utxo_cell0.innerHTML = address_index_map[json_response[j - 1].address];
            utxo_cell1.innerHTML = json_response[j - 1].address;
            utxo_cell2.innerHTML = json_response[j - 1].amount;
            utxo_amount = utxo_amount + json_response[j - 1].amount;
        }
    }
    unspent_transaction_amount.innerHTML = utxo_amount;
}

function block_count(xhr) {
    var json_response = JSON.parse(xhr.response);
    // console.log(json_response);
    document.getElementById("td_block_count").innerHTML = json_response.block_count;
}

function charts_fn(xhr) {
    var json_response = JSON.parse(xhr.response);
    console.log(json_response);
    var balances = json_response.balances,
        xValues = [],
        k;
    for (k = 0; k < balances.length; k++) {
        xValues.push(k);
    }
    var CANVAS_CHART_BALANCES = document.getElementById("balances_chart_new");
    var CANVAS_CHART_BLOCKS = document.getElementById("blocks_chart_new");
    var lineChart_1 = new Chart(CANVAS_CHART_BALANCES, {
        type: 'line',
        data: {
            labels: xValues,
            datasets: [{
                label: "Balances (x-axis: Node, y-axis: Balance)",
                fill: false,
                lineTension: 0,
                borderColor: "rgba(75, 192, 192, 1)",
                data: balances
            }],
            options: {
                animation: false
            }
        }
    });
    var lineChart_2 = new Chart(CANVAS_CHART_BLOCKS, {
        type: 'line',
        data: {
            labels: xValues,
            datasets: [{
                label: "Number of blocks mined by each node",
                fill: false,
                lineTension: 0,
                borderColor: "rgba(75, 72, 192, 1)",
                data: json_response.num_blocks_mined_by_each
            }]
        }
    });

}

function updateSimulation() {
    ajaxCalls("/updated_simulation", table_nodes);
    ajaxCalls("/num_transactions", num_transactions);
    ajaxCalls("/mined_bitcoin_balance", mined_bitcoin_balance);
    ajaxCalls("/transacted_bitcoin_balance", transacted_bitcoin_balance);
    ajaxCalls("/get_utxos", utxos);
    ajaxCalls("/get_block_count", block_count);
    if (request_charts) {
        ajaxCalls("/get_charts", charts_fn);
    }
}

setInterval(updateSimulation, 2000);

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"