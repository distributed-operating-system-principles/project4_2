defmodule Project4_1.Keys do
  @limit :binary.decode_unsigned(<<
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFF,
           0xFE,
           0xBA,
           0xAE,
           0xDC,
           0xE6,
           0xAF,
           0x48,
           0xA0,
           0x3B,
           0xBF,
           0xD2,
           0x5E,
           0x8C,
           0xD0,
           0x36,
           0x41,
           0x41
         >>)

  def create_pair() do
    private_key = create_private_key()
    public_key = create_public_key(private_key)
    {:ok, private_key, public_key}
  end

  def sign(message, private_key) do
    :crypto.sign(:ecdsa, :sha256, message, [private_key, :secp256k1])
  end

  def verify(message, signature, public_key) do
    :crypto.verify(:ecdsa, :sha256, message, signature, [public_key, :secp256k1])
  end

  def get_hash(message) do
    :sha256 |> :crypto.hash(:sha256 |> :crypto.hash(message))
  end

  def get_address(public_key) do
    :crypto.hash(:ripemd160, :crypto.hash(:sha256, public_key))
  end

  defp is_valid(key) when is_binary(key) do
    is_valid(:binary.decode_unsigned(key))
  end

  defp is_valid(key) when key > 1 and key < @limit, do: true

  defp is_valid(_key), do: false

  defp create_private_key() do
    private_key = :crypto.strong_rand_bytes(32)

    case is_valid(private_key) do
      true -> private_key
      false -> create_private_key()
    end
  end

  defp create_public_key(private_key) do
    :crypto.generate_key(:ecdh, :crypto.ec_curve(:secp256k1), private_key) |> elem(0)
  end
end
