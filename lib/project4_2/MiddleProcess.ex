defmodule Project4_1.MiddleProcess do
  use GenServer
  @num_transactions 5

  def start_link(_init_state \\ []) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(nil) do
    state = %{}
    state = Map.put(state, :transaction_map, %{})
    {:ok, state}
  end

  def handle_call({:set_num_nodes, num_nodes}, _from, state) do
    state = Map.put(state, :num_nodes, num_nodes)
    {:reply, :ok, state}
  end

  def handle_call({:submit_transaction, from_index, to_index, amount}, _from, state) do
    list_pids = state[:list_pids]

    Project4_1.BitcoinNode.send_money(
      Enum.at(list_pids, from_index - 1),
      Enum.at(list_pids, to_index - 1),
      amount
    )

    {:reply, :ok, state}
  end

  def handle_call(:get_list_pids, _from, state) do
    {:reply, Map.get(state, :list_pids), state}
  end

  def handle_call(:start_nodes, _from, state) do
    num_nodes = Map.get(state, :num_nodes)

    list_pids =
      for i <- 1..num_nodes do
        {:ok, pid} = Project4_1.BitcoinNode.start_link({self(), i})
        pid
      end

    for pid <- list_pids do
      Project4_1.BitcoinNode.set_list_pids(pid, list_pids)
    end

    state = Map.put(state, :list_pids, list_pids)

    {:reply, list_pids, state}
  end

  def handle_call(:get_num_transactions, _from, state) do
    {:reply, GenServer.call(Enum.at(state[:list_pids], 0), :get_num_transactions), state}
  end

  def handle_call(:get_mined_bitcoin_balance, _from, state) do
    {:reply, GenServer.call(Enum.at(state[:list_pids], 0), :get_mined_bitcoin_balance), state}
  end

  def handle_call(:get_utxos, _from, state) do
    {:reply, GenServer.call(Enum.at(state[:list_pids], 0), :get_utxos), state}
  end

  def handle_call(:get_transacted_bitcoin_balance, _from, state) do
    {:reply, GenServer.call(Enum.at(state[:list_pids], 0), :get_transacted_bitcoin_balance),
     state}
  end

  def handle_call(:get_block_count, _from, state) do
    {:reply, GenServer.call(Enum.at(state[:list_pids], 0), :get_block_count), state}
  end

  def handle_cast(:kill_all_nodes, state) do
    list_pids = Map.get(state, :list_pids)

    if(list_pids !== nil) do
      for pid <- list_pids do
        Process.exit(pid, :kill)
      end
    end

    {:noreply, state}
  end

  def handle_cast(:start_transactions, state) do
    {:noreply, state}
  end

  def handle_info(:start_random_transactions, state) do
    list_pids = state[:list_pids]
    created_transaction = false

    for pid <- list_pids do
      balance = GenServer.call(pid, :get_wallet_balance)

      if(!created_transaction && (balance / 10) |> Kernel.trunc() > 0) do
        Project4_1.BitcoinNode.send_money(
          pid,
          Enum.at(list_pids, :rand.uniform(length(list_pids) - 1)),
          (balance / 10) |> Kernel.trunc()
        )
      end
    end

    Process.send_after(self(), :start_random_transactions, 5000)

    {:noreply, state}
  end

  def handle_info({:genesis_block, pid}, state) do
    {:noreply, Map.put(state, :genesis_block_pid, pid)}
  end
end
