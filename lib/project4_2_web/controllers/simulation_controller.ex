defmodule Project42Web.SimulationController do
  use Project42Web, :controller

  def index(conn, %{"num_nodes" => num_nodes, "transactions_type" => transactions_type}) do
    # text(conn, "Hello!")
    middle_process_pid = Process.whereis(Project4_1.MiddleProcess)

    if(middle_process_pid !== nil) do
      GenServer.cast(Project4_1.MiddleProcess, :kill_all_nodes)
      Process.unregister(Project4_1.MiddleProcess)
      Process.exit(middle_process_pid, :kill)
      Project4_1.MiddleProcess.start_link()
    else
      Project4_1.MiddleProcess.start_link()
    end

    # IO.puts("REACHED HERE")
    show_transaction_form =
      if(transactions_type === "automatic") do
        Process.send_after(Project4_1.MiddleProcess, :start_random_transactions, 10000)
        false
      else
        true
      end

    :ok =
      GenServer.call(
        Project4_1.MiddleProcess,
        {:set_num_nodes, elem(Integer.parse(num_nodes), 0)}
      )

    _list_pids = GenServer.call(Project4_1.MiddleProcess, :start_nodes)

    conn
    |> redirect(to: "/simulation?show_transaction_form=" <> to_string(show_transaction_form))
  end

  def simulation(conn, %{"show_transaction_form" => show_transaction_form}) do
    list_pids = GenServer.call(Project4_1.MiddleProcess, :get_list_pids)

    list_table =
      Enum.reduce(list_pids, [], fn pid, list_table ->
        list_item = %{}

        list_item =
          Map.put(
            list_item,
            :address,
            GenServer.call(pid, :get_wallet_address) |> Base.encode16()
          )

        list_item = Map.put(list_item, :balance, GenServer.call(pid, :get_wallet_balance))
        list_table ++ [list_item]
      end)

    conn
    |> assign(:num_pids, length(list_pids))
    |> assign(:list_table, list_table)
    |> assign(:show_transaction_form, show_transaction_form)
    |> render("index.html")
  end

  def updated_simulation(conn, _params) do
    list_pids = GenServer.call(Project4_1.MiddleProcess, :get_list_pids)

    list_table =
      Enum.reduce(list_pids, [], fn pid, list_table ->
        list_item = %{}

        list_item =
          Map.put(
            list_item,
            :address,
            GenServer.call(pid, :get_wallet_address) |> Base.encode16()
          )

        list_item = Map.put(list_item, :balance, GenServer.call(pid, :get_wallet_balance))
        list_table ++ [list_item]
      end)

    json(conn, list_table)
  end

  def num_transactions(conn, _params) do
    num_transactions = GenServer.call(Project4_1.MiddleProcess, :get_num_transactions)
    json(conn, %{num_transactions: num_transactions})
  end

  def mined_bitcoin_balance(conn, _params) do
    mined_bitcoin_balance = GenServer.call(Project4_1.MiddleProcess, :get_mined_bitcoin_balance)
    json(conn, %{mined_bitcoin_balance: mined_bitcoin_balance})
  end

  def transacted_bitcoin_balance(conn, _params) do
    transacted_bitcoin_balance =
      GenServer.call(Project4_1.MiddleProcess, :get_transacted_bitcoin_balance)

    json(conn, %{transacted_bitcoin_balance: transacted_bitcoin_balance})
  end

  def get_utxos(conn, _params) do
    utxos = GenServer.call(Project4_1.MiddleProcess, :get_utxos)

    utxos =
      for {_key, utxo} <- utxos do
        %{amount: utxo[:amount], address: Base.encode16(utxo[:address])}
      end

    json(conn, utxos)
  end

  def get_block_count(conn, _params) do
    block_count = GenServer.call(Project4_1.MiddleProcess, :get_block_count)
    json(conn, %{block_count: block_count})
  end

  def submit_transaction(conn, %{
        "from_index" => from_index,
        "to_index" => to_index,
        "amount" => amount
      }) do
    :ok =
      GenServer.call(
        Project4_1.MiddleProcess,
        {:submit_transaction, Integer.parse(from_index) |> elem(0),
         Integer.parse(to_index) |> elem(0), Integer.parse(amount) |> elem(0)}
      )

    conn
    |> redirect(to: "/simulation?show_transaction_form=true")
  end

  def get_charts(conn, _params) do
    list_pids = GenServer.call(Project4_1.MiddleProcess, :get_list_pids)

    balances =
      for pid <- list_pids do
        GenServer.call(pid, :get_wallet_balance)
      end

    num_blocks_mined_by_each =
      for pid <- list_pids do
        GenServer.call(pid, :get_num_blocks_mined_by_me)
      end

    res = %{balances: balances, num_blocks_mined_by_each: num_blocks_mined_by_each}
    json(conn, res)
  end
end
