defmodule Project42Web.Router do
  use Project42Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Project42Web do
    pipe_through :browser

    get "/", PageController, :index

    get "/start_simulation/", SimulationController, :index

    get "/simulation/", SimulationController, :simulation

    get "/submit_transaction/", SimulationController, :submit_transaction

    get "/updated_simulation/", SimulationController, :updated_simulation

    get "/num_transactions", SimulationController, :num_transactions

    get "/mined_bitcoin_balance", SimulationController, :mined_bitcoin_balance

    get "/transacted_bitcoin_balance", SimulationController, :transacted_bitcoin_balance

    get "/get_utxos", SimulationController, :get_utxos

    get "/get_charts", SimulationController, :get_charts

    get "/get_block_count", SimulationController, :get_block_count
  end

  # Other scopes may use custom stacks.
  # scope "/api", Project42Web do
  #   pipe_through :api
  # end
end
